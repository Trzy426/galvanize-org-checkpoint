package com.galvanize;

import formatters.Formatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static com.galvanize.Application.getFormatter;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FormatterTest {

    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void FormatterHTMLTest() {
        String expected = "<dl>\n" +
                "  <dt>Type</dt><dd>Suite</dd>\n" +
                "  <dt>Room Number</dt><dd>14</dd>\n" +
                "  <dt>Start Time</dt><dd>10:30AM</dd>\n" +
                "  <dt>End Time</dt><dd>12:30AM</dd>\n" +
                "</dl>";
        Booking trip = Booking.parse("s14-10:30AM-12:30AM");
        Formatter actual = getFormatter("html");
        assertEquals(expected, actual.format(trip));

    }
    @Test
    public void FormatterCSV() {
        String expected =
                "type,room number,start time,end time\n" +
                        "Suite,14,10:30AM,12:30AM";


        Booking trip = Booking.parse("s14-10:30AM-12:30AM");
        Formatter actual = getFormatter("csv");
        assertEquals(expected, actual.format(trip));

    }
    @Test
    public void FormatterJSON() {
        String expected = "{\n" +
                        "  \"type\": \"Suite\",\n" +
                        "  \"roomNumber\": 14,\n" +
                        "  \"startTime\": \"10:30AM\",\n" +
                        "  \"endTime\": \"12:30AM\"\n" +
                        "}";


        Booking trip = Booking.parse("s14-10:30AM-12:30AM");
        Formatter actual = getFormatter("json");
        assertEquals(expected, actual.format(trip));

    }


}
